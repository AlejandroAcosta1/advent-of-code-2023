const readFile = require("../utils/read-file");

readFile().then(cube);

function cube(data) {
    const redCount = 12
    const greenCount = 13
    const blueCount = 14;

    const games = data.split(/[\r\n]+/).filter(x => x);
    const gameStats = games.map(game => {
        const [rawGameId, roundsString] = game.split(": ");
        const [, gameId] = rawGameId.split(" ")
        const rawRounds = roundsString.split("; ")
        const rounds = rawRounds.map(round => round.split(", "))

        const gameStatus = rounds.reduce((gameStats, round) => {
            const roundStats = round.reduce((roundStats, turn) => {
                const [count, color] = turn.split(" ")
                if (!roundStats[color] || roundStats[color] < count) {
                    roundStats[color] = parseInt(count)
                }

                return roundStats;
            }, {})

            if ((!gameStats.red && roundStats.red) || roundStats.red > gameStats.red) {
                gameStats.red = roundStats.red
            }
            if ((!gameStats.green && roundStats.green) || roundStats.green > gameStats.green) {
                gameStats.green = roundStats.green
            }
            if ((!gameStats.blue && roundStats.blue) || roundStats.blue > gameStats.blue) {
                gameStats.blue = roundStats.blue
            }

            return gameStats;
        }, {})

        const isPossible = gameStatus.red <= redCount && gameStatus.green <= greenCount && gameStatus.blue <= blueCount;
        const power = gameStatus.red * gameStatus.green * gameStatus.blue;

        return { id: parseInt(gameId), status: isPossible, power }
    });

    const possibleGameSum = gameStats.reduce((sum, stats) => {
        if (stats.status) {
            return sum + stats.id;
        }
        return sum;
    }, 0)

    const powerSum = gameStats.reduce((sum, stats) => {
        return sum + stats.power;
    }, 0)

    console.log(`The sume of the ID of the possible games is ${possibleGameSum}`)
    console.log(`The sum of the power of the games is ${powerSum}`)
}