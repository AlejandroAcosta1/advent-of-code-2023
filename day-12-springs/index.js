const readFile = require("../utils/read-file");
const { parseLines } = require("../utils/input-data");
const { arraySum, parseInt10 } = require("../utils/numbers");

const OPERATIONAL = ".";
const DAMAGED = "#";
const UNKNOWN = "?";

readFile().then(springReport);

function springReport(data) {
  const springReport = parseLines(data).map((line) => line.split(" "));
  const unfoldedSpringReport = unfoldReport(springReport);

  const configurationCount = countConfigurations(springReport);
  const unfoldedConfigurationCount =
    countConfigurations(unfoldedSpringReport);

  console.log(`The sum of the counts is ${configurationCount}`);
  console.log(
    `The new sum of possible arrangements is ${unfoldedConfigurationCount}`
  );
}

function unfoldReport(springReport, copies = 5) {
  return springReport.map(([status, checksum]) => {
    let newStatus = status + ("?" + status).repeat(copies - 1);
    let newChecksum = checksum + ("," + checksum).repeat(copies - 1);
    return [collapseWorkingSprings(newStatus), newChecksum];
  });
}

function countConfigurations(report) {
  return report.reduce((sum, [springStatus, checksum]) => {
    return sum + count(springStatus, checksum.split(',').map(parseInt10));
  }, 0)
}

{
  cache = {}
  // Adapted from https://www.reddit.com/r/adventofcode/comments/18hbbxe/2023_day_12python_stepbystep_tutorial_with_bonus/
  function count(status, checksum) {
    const checksumCacheKey = checksum.join(',');
    if (cache?.[status]?.[checksumCacheKey] !== undefined) {
      return cache[status][checksumCacheKey];
    }
    // No more groups to evaluate
    if (!checksum || !checksum.length) {
      // There are no more damaged springs
      if (status.indexOf(DAMAGED) === -1) {
        return 1;
      }
      // There are more damaged springs
      else {
        return 0;
      }
    }

    // There are more groups, but there is no more springs to fit it
    if (!status) {
      return 0;
    }

    const nextCharacter = status[0]
    const nextGroup = checksum[0]

    let amount = 0;
    if (nextCharacter === DAMAGED) {
      amount += damaged();
    }
    else if (nextCharacter === OPERATIONAL) {
      amount += operational();
    }
    else if (nextCharacter === UNKNOWN) {
      amount += damaged() + operational();
    }
    else {
      console.error("This should not happen")
      console.error({ status, checksum })
    }
    return amount;

    function operational() {
      // Ignore the character and look at the next substring
      const nextStatus = status.substr(1)
      if (!cache[nextStatus]) {
        cache[nextStatus] = {}
      }
      const nextValue = count(nextStatus, checksum);
      cache[nextStatus][checksumCacheKey] = nextValue;
      return nextValue;
    }

    function damaged() {
      const realCurrentGroup = status.substr(0, nextGroup)
      const currentGroup = realCurrentGroup.replace(/[?]/g, DAMAGED);

      // The next group can't fit all damaged springs
      if (currentGroup !== DAMAGED.repeat(nextGroup)) {
        return 0;
      }

      // The rest of the status is just the last group
      if (status.length === nextGroup) {
        // make sure this is the last group
        if (checksum.length === 1) {
          // This is a valid possibility
          return 1
        }
        else {
          // There are no more groups so this isn't possible
          return 0
        }
      }

      // Make sure next character can separate groups
      if ([UNKNOWN, OPERATIONAL].includes(status[nextGroup])) {
        // It can be a separator, so continue
        const nextStatus = status.substr(nextGroup + 1)
        const nextValue = count(nextStatus, checksum.slice(1));
        if (!cache[nextStatus]) {
          cache[nextStatus] = {};
        }
        cache[nextStatus][checksum.slice(1).join(',')] = nextValue;
        return nextValue
      }

      // No possibilities
      return 0
    }
  }
}

function collapseWorkingSprings(configuration) {
  return configuration.replace(/\.+/g, ".").replace(/^\.|\.$/, "");
}
