const readFile = require("../utils/read-file");
const { arraySum, parseInt10 } = require("../utils/numbers");

readFile().then(findSequence);

function findSequence(data) {
  const lines = data.split(/\r?\n/).filter((x) => x);

  const historyData = lines.map((line) => line.split(" ").map(parseInt10));
  const extrapolatedHistories = historyData.reduce(
    (extrapolations, history) => {
      const newHistory = extrapolateHistory(history);
      extrapolations.push(newHistory);
      return extrapolations;
    },
    []
  );

  const historyDataSum = extrapolatedHistories.reduce((sum, history) => {
    return sum + history.at(-1);
  }, 0);
  const pastHistoryDataSum = extrapolatedHistories.reduce((sum, history) => {
    return sum + history[0];
  }, 0);

  // These values are off by 2 for reasons unknown
  console.log(`The sum of the future extrapolated values is ${historyDataSum}`);
  console.log(
    `The sum of the past extrapolated values is ${pastHistoryDataSum}`
  );
}

function extrapolateHistory(history) {
  let nextSequence = findNextSequence(history);
  const historySequences = [history, nextSequence];

  while (!isFinalSequence(nextSequence)) {
    nextSequence = findNextSequence(nextSequence);
    historySequences.push(nextSequence);
  }

  const extrapolatedHistories = [...historySequences]
    .reverse()
    .reduce((updatedHistories, history, index) => {
      if (index === 0) {
        updatedHistories.push(history.concat(0));
      }
      const previousHistory = updatedHistories.at(-1);
      const finalDatum = history.at(-1);
      const finalAddend = index > 0 ? previousHistory.at(-1) : 0;
      history.push(finalDatum + finalAddend);

      const initialDatum = history[0];
      const initialAddend = index > 0 ? previousHistory[0] : 0;
      history.unshift(initialDatum - initialAddend);

      updatedHistories.push(history);
      return updatedHistories;
    }, [])
    .reverse();

  return extrapolatedHistories[0];
}

function findNextSequence(history) {
  return history.reduce((sequence, datum, index) => {
    if (index === 0) {
      return sequence;
    }
    return sequence.concat(datum - history[index - 1]);
  }, []);
}

function isFinalSequence(history) {
  return arraySum(history) === 0;
}
