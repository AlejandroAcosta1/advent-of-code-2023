const readFile = require("../utils/read-file");
const { parseLines, parseLineGroups } = require("../utils/input-data");
const { arraySum, parseInt10 } = require("../utils/numbers");
const { group } = require("console");

const ASH = ".";
const ROCKS = "#";

// need to find row/columns of reflection
readFile().then((data) => {
  const groups = parseLineGroups(data);

  const mirrorLocations = findMirrors(groups);
  console.log({ mirrorLocations });

  const groupIdentity = mirrorLocations.reduce(
    (sum, { mirrorColumn = 0, mirrorRow = 0 }) =>
      sum + mirrorColumn + 100 * mirrorRow,
    0
  );

  console.log(`The number you get after summarizing notes is ${groupIdentity}`);
});

function findMirrors(patterns) {
  return patterns.map(findPattern);
}

function findPattern(pattern) {
  const mirrorRow = findInflectionPoint(pattern);
  if (mirrorRow) {
    return { mirrorRow };
  }

  const transposedPattern = transposePattern(pattern);
  const mirrorColumn = findInflectionPoint(transposedPattern);

  console.log({ mirrorColumn });

  if (mirrorColumn) {
    return { mirrorColumn };
  }

  console.error("This shouldn't happen");
}

function findInflectionPoint(pattern) {
  const seenRows = {};
  pattern.forEach((row, index) => {
    const rowIndex = index + 1;
    if (!seenRows[row]) {
      seenRows[row] = {}
    }
    seenRows[row][rowIndex] = true;
  });

  let isReflection = false;
  let inflectionPoint = undefined;
  let reflected;
  for (let index = 0; index < pattern.length; index++) {
    const row = pattern[index];
    const duplicateRows = Object.keys(seenRows[row]).map(parseInt10);

    // Skip row if there are no duplicates and not in a possible reflection
    if (!isReflection && duplicateRows.length === 1) {
      continue;
    }
    // Terminate possible reflection
    else if (isReflection && duplicateRows.length === 1) {
      isReflection = false;
      console.log('Terminate')
      break;
    }

    // Start checking for reflection
    if (!isReflection) {
      const containsStart = duplicateRows.indexOf(1) > -1;
      const containsEnd = duplicateRows.indexOf(pattern.length) > -1;

      if (containsStart || containsEnd) {
        isReflection = true;
      }
      else {
        break;
      }

      if (containsStart) {
        reflected = Math.max(...duplicateRows);
      }
      else if (containsEnd) {
        reflected = pattern.length;
      }
    }
    else {
      // Need to handle more than first iteration
      const nextReflection = reflected - 1;
      const hasNextReflection = duplicateRows.includes(nextReflection)
      const isInflectionPoint = nextReflection === index;

      if (isInflectionPoint) {
        inflectionPoint = index;
        break;
      }

      if (hasNextReflection) {
        // We can continue. 
        reflected = nextReflection;
      }
    }
  }

  return inflectionPoint;
}

function transposePattern(pattern) {
  let newPattern = [];

  for (let i = 0; i < pattern.length; i++) {
    for (let j = 0; j < pattern[0].length; j++) {
      if (!newPattern[j]) {
        newPattern[j] = "";
      }
      newPattern[j] += pattern[i][j];
    }
  }

  return newPattern;
}

function transposeUniverse(universe) {
  const transposedUniverse = [];

  universe.forEach((_row, y) => {
    universe[y].forEach((cell, x) => {
      if (!transposedUniverse[x]) {
        transposedUniverse[x] = [];
      }
      transposedUniverse[x][y] = cell;
    });
  });

  return transposedUniverse;
}
