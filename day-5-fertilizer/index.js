const readFile = require("../utils/read-file");
const { parseInt10 } = require("../utils/numbers");

readFile().then(fertilizer);

function fertilizer(data) {
  const [rawSeeds, ...rawMappings] = data.split(/(\r?\n){2}/).filter((x) => x.match(/\w/));

  const seeds = rawSeeds.split(": ")[1].split(" ").map(parseInt10);

  const seedRanges = [];
  for (let i = 0; i < seeds.length; i++) {
    if (i % 2 === 0) {
      seedRanges.push([seeds[i]])
    }
    else {
      seedRanges[Math.floor(i / 2)].push(seeds[i])
    }
  }

  const allMappings = rawMappings.reduce((accumulator, mappingString) => {
    const [mappingName, ...mappings] = mappingString.split(/\r?\n/);
    const [source, , destination] = mappingName.split(" ")[0].split("-");
    const botanyMapping = {
      source,
      destination,
      mapping: {},
    };

    mappings.forEach((mapping) => {
      [destinationRangeStart, sourceRangeStart, rangeLength] =
        mapping.split(" ").map(parseInt10);

      botanyMapping.mapping[sourceRangeStart] = {
        range: rangeLength,
        destination: destinationRangeStart,
        source: sourceRangeStart,
      }
    });

    return Object.assign({}, accumulator, { [source]: botanyMapping });
  }, {});

  const seedLocations = seeds
    .map((seed) => findSeedLocation(allMappings, seed));

  const realSeedLocation = lowestSeedRangeFinder(allMappings, seedRanges)

  const lowestSeedLocation = seedLocations.reduce((min, location) => Math.min(min, location))

  console.log(`The lowest location corresponding to the starting seeds is ${lowestSeedLocation}`)
  console.log(`The lowest location with ranges is ${realSeedLocation}`)
}

function findSeedLocation(mappings, seed) {
  const targetDestination = "location";

  let destinationMap = mappings.seed;
  let destination = destinationMap.destination;
  let source = destinationMap.source;
  let sourceValue = seed;
  let destinationValue = destinationMap.mapping[sourceValue] ?? sourceValue;

  // console.log(`Seeking location for seed ${seed}`)
  while (source !== targetDestination) {
    const mapping = destinationMap.mapping;
    // console.log(`My ${source} (${sourceValue}) wants a ${destination}.`)

    destinationValue = Object.values(mapping).reduce((targetValue, { source, destination, range }) => {
      if (sourceValue >= source && sourceValue < source + range) {
        const difference = sourceValue - source
        return destination + difference;
      }
      return targetValue
    }, sourceValue)

    // console.log(`Found a ${destination} (${destinationValue})`)

    if (destination === targetDestination) {
      break;
    }

    destinationMap = mappings[destination];
    destination = destinationMap.destination;
    source = destinationMap.source;
    sourceValue = destinationValue;
  }

  return destinationValue;
}

function lowestSeedRangeFinder(mappings, seedRanges) {
  const lowestSeed = seedRanges.reduce((lowestFoundLocation, seedRange) => {
    const [rangeStart, rangeLength] = seedRange
    for (let seed = rangeStart; seed < rangeStart + rangeLength; seed++) {
      const location = findSeedLocation(mappings, seed);
      lowestFoundLocation = Math.min(lowestFoundLocation, location)
    }
    return lowestFoundLocation;
  }, Number.MAX_SAFE_INTEGER)
  return lowestSeed
}