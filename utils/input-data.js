const newlinePattern = "\n";

function parseLines(data) {
  return data
    .split(new RegExp(newlinePattern))
    .filter((x) => x);
}

function parseLineGroups(data) {
  return data
    .split(new RegExp(`${newlinePattern}{2}`))
    .map(parseLines);
}

function parseGrid(data) {
  return parseLines(data).map((line) => line.split(""));
}

function gridToString(grid) {
  return grid.reduce(
    (fullString, row) =>
      fullString + row.reduce((string, cell) => string + cell, "") + "\n",
    ""
  );
}

module.exports = {
  parseLines,
  parseGrid,
  gridToString,
  parseLineGroups,
};
