function parseInt10(x) {
  return parseInt(x, 10);
}

function arraySum(array) {
  return array.reduce((sum, addend) => sum + addend);
}

function arrayProduct(array) {
  return array.reduce((product, multiplicand) => product * multiplicand);
}

module.exports = {
  arrayProduct,
  arraySum,
  parseInt10,
};
