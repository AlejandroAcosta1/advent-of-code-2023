const fs = require("fs/promises");
const args = process.argv.slice(2);
const inputFile = args[0];

module.exports = async function readFile() {
  try {
    const data = await fs.readFile(inputFile, { encoding: "utf8" });
    return data;
  } catch (err) {
    console.error(err);
  }
};
