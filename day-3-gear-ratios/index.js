const readFile = require("../utils/read-file");

readFile().then(gears);

function gears(data) {
  const schematic = data.split(/[\r\n]+/).filter((x) => x);

  const matchPartIds = /[^a-zA-Z0-9.]/g;
  const partCoordinates = findMatchingCoordinates(schematic, matchPartIds);

  const matchGears = /[*]/g;
  const gearCoordinates = findMatchingCoordinates(schematic, matchGears);

  const partNumbers = partCoordinates.reduce(
    (numbersAccumulator, coordinate) => {
      const numbers = findPartNumbers(schematic, coordinate);

      return numbersAccumulator.concat(numbers);
    },
    []
  );

  const gearRatios = gearCoordinates.reduce((ratios, coordinate) => {
    const gearRatios = findPartNumbers(schematic, coordinate);

    if (gearRatios.length < 2) {
      return ratios;
    }

    return ratios.concat(gearRatios[0] * gearRatios[1]);
  }, []);

  const partSum = partNumbers.reduce((sum, addend) => sum + addend);

  const gearSum = gearRatios.reduce((sum, addend) => sum + addend);

  console.log(`The sum of the part numbers is ${partSum}.`);
  console.log(`The sum of the gear ratios is ${gearSum}.`);
}

// Parsing order means y and x are inverted when accessing schematic.
// e.g. schematic[y][x]
function findMatchingCoordinates(schematic, pattern) {
  return schematic.reduce((accumulator, row, y) => {
    let match;
    while ((match = pattern.exec(row))) {
      const x = match.index;
      accumulator.push([x, y]);
    }
    return accumulator;
  }, []);
}

function findPartNumbers(schematic, coordinate) {
  const [partX, partY] = coordinate;

  const firstRow = partX === 0;
  const lastRow = partX === schematic.length;
  const firstColumn = partY === 0;
  const lastColumn = partY === schematic[0].length;

  const partNumbers = [];

  // Diagonal up-left
  if (
    !firstRow &&
    !firstColumn &&
    schematic[partY - 1][partX - 1].match(/\d/)
  ) {
    const partNumber = scanForNumber(schematic, [partX - 1, partY - 1]);
    partNumbers.push(partNumber);
  }

  // Above
  if (!firstRow && schematic[partY - 1][partX].match(/\d/)) {
    const partNumber = scanForNumber(schematic, [partX, partY - 1]);
    partNumbers.push(partNumber);
  }

  // Diagonal up-right
  if (!firstRow && !lastColumn && schematic[partY - 1][partX + 1].match(/\d/)) {
    const partNumber = scanForNumber(schematic, [partX + 1, partY - 1]);
    partNumbers.push(partNumber);
  }

  // Left
  if (!firstColumn && schematic[partY][partX - 1].match(/\d/)) {
    const partNumber = scanForNumber(schematic, [partX - 1, partY]);
    partNumbers.push(partNumber);
  }

  // Right
  if (!lastColumn && schematic[partY][partX + 1].match(/\d/)) {
    const partNumber = scanForNumber(schematic, [partX + 1, partY]);
    partNumbers.push(partNumber);
  }

  // Diagonal down-left
  if (!lastRow && !firstColumn && schematic[partY + 1][partX - 1].match(/\d/)) {
    const partNumber = scanForNumber(schematic, [partX - 1, partY + 1]);
    partNumbers.push(partNumber);
  }

  // Below
  if (!lastRow && schematic[partY + 1][partX].match(/\d/)) {
    const partNumber = scanForNumber(schematic, [partX, partY + 1]);
    partNumbers.push(partNumber);
  }

  // Diagonal down-right
  if (!lastRow && !lastColumn && schematic[partY + 1][partX + 1].match(/\d/)) {
    const partNumber = scanForNumber(schematic, [partX + 1, partY + 1]);
    partNumbers.push(partNumber);
  }

  const dedupedPartNumbers = new Set(partNumbers);
  return [...dedupedPartNumbers];
}

/**
 * Parse the number from the coordinate
 * If we're in the first column, just parseInt; it'll get the number
 * If one column left is a number, recursively check one further
 * If one column left is not a number, parse the substring from our position; it'll get the number
 */
function scanForNumber(schematic, coordinate) {
  const [x, y] = coordinate;

  if (x === 0) {
    return parseInt(schematic[y]);
  }

  if (schematic[y][x - 1].match(/\d/)) {
    return scanForNumber(schematic, [x - 1, y]);
  } else {
    const row = schematic[y];
    const numberString = row.substring(x);
    return parseInt(numberString);
  }
}
