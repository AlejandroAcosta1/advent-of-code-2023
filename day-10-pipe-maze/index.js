const { parseGrid } = require("../utils/input-data");
const readFile = require("../utils/read-file");

readFile().then(pipeMaze);

const NORTH = "N";
const SOUTH = "S";
const WEST = "W";
const EAST = "E";
const NS = "|";
const EW = "-";
const NE = "L";
const NW = "J";
const SW = "7";
const SE = "F";
const GROUND = ".";
const START = "S";

const NE_LOOP_BORDER = "NE";
const SE_LOOP_BORDER = "SE";

function pipeMaze(data) {
  const grid = parseGrid(data);

  const startPoint = findStartingPoint(grid, data);
  const startShape = findStartShape(grid, startPoint);
  const { maxDistance, seenPoints } = findFurthestDistance(
    grid,
    startPoint,
    startShape
  );

  const realGrid = parseGrid(data.replace(START, startShape));
  const innerArea = findInnerArea(realGrid, seenPoints);

  console.log(
    `The number of steps it takes to get from the starting position to the point furthest from the starting point is ${maxDistance}`
  );
  console.log(`The number of tiles enclosed by the loop is ${innerArea}`);
}

function findInnerArea(grid, seenPoints) {
  let innerPoints = 0;
  let isInsideLoop = false;
  let state = null;
  grid.forEach((row, y) => {
    row.forEach((cell, x) => {
      const isLoopPipe = containsPoint(seenPoints, [x, y]);
      if (!isLoopPipe) {
        innerPoints += isInsideLoop ? 1 : 0;
        return;
      }

      const gridShape = accessGrid(grid, x, y);
      switch (gridShape) {
        case NS:
          isInsideLoop = !isInsideLoop;
          break;
        case NE:
          state = NE_LOOP_BORDER;
          break;
        case SE:
          state = SE_LOOP_BORDER;
          break;
        case NW:
          if (state === SE_LOOP_BORDER) {
            isInsideLoop = !isInsideLoop;
            state = null;
          }
          break;
        case SW:
          if (state === NE_LOOP_BORDER) {
            isInsideLoop = !isInsideLoop;
            state = null;
          }
          break;
        case EW:
          // console.log("Horizontal pipe; do nothing");
          break;
      }
    });
  });

  return innerPoints;
}

function findFurthestDistance(grid, startingPoint, startShape) {
  let currentPoint = startingPoint;
  let nextPoint = findStartingNextPoint(startingPoint, startShape);
  let seenPoints = [startingPoint];

  while (!containsPoint(seenPoints, nextPoint)) {
    seenPoints.push(nextPoint);

    currentShape = accessGrid(grid, ...nextPoint);

    nextPoint = findNextPoint(currentPoint, nextPoint, currentShape);
    currentPoint = seenPoints.at(-1);
  }

  return { maxDistance: Math.floor(seenPoints.length / 2), seenPoints };
}

function findNextPoint(previousPoint, currentPoint, previousShape) {
  switch (previousShape) {
    case NS:
      if (pointEquals(addPoint(previousPoint, NORTH), currentPoint)) {
        return addPoint(currentPoint, NORTH);
      } else {
        return addPoint(currentPoint, SOUTH);
      }
    case EW:
      if (pointEquals(addPoint(previousPoint, EAST), currentPoint)) {
        return addPoint(currentPoint, EAST);
      } else {
        return addPoint(currentPoint, WEST);
      }
    case NE:
      if (pointEquals(addPoint(previousPoint, WEST), currentPoint)) {
        return addPoint(currentPoint, NORTH);
      } else {
        return addPoint(currentPoint, EAST);
      }
    case NW:
      if (pointEquals(addPoint(previousPoint, EAST), currentPoint)) {
        return addPoint(currentPoint, NORTH);
      } else {
        return addPoint(currentPoint, WEST);
      }
    case SW:
      if (pointEquals(addPoint(previousPoint, EAST), currentPoint)) {
        return addPoint(currentPoint, SOUTH);
      } else {
        return addPoint(currentPoint, WEST);
      }
    case SE:
      if (pointEquals(addPoint(previousPoint, WEST), currentPoint)) {
        return addPoint(currentPoint, SOUTH);
      } else {
        return addPoint(currentPoint, EAST);
      }
  }
}

// Choose starting direction for beginning of grid navigation
function findStartingNextPoint([startX, startY], startShape) {
  switch (startShape) {
    case NS:
      return [startX, startY - 1];
    case EW:
      return [startX + 1, startY];
    case NW:
      return [startX, startY - 1];
    case NE:
      return [startX, startY - 1];
    case SW:
      return [startX, startY + 1];
    case SE:
      return [startX, startY + 1];
    default:
      console.error("Could not find starting point direction");
  }
}

function pointEquals([x1, y1], [x2, y2]) {
  return x1 === x2 && y1 === y2;
}

function addPoint([x, y], direction) {
  if (direction === NORTH) {
    return [x, y - 1];
  } else if (direction === SOUTH) {
    return [x, y + 1];
  } else if (direction === WEST) {
    return [x - 1, y];
  } else if (direction === EAST) {
    return [x + 1, y];
  }
}

function findStartShape(grid, startPoint) {
  const [x, y] = startPoint;
  const gridHeight = grid.length;
  const gridWidth = grid[0].length;

  const connections = [];

  // console.log({ grid, x, y });
  const westPipe = x > 0 ? accessGrid(grid, x - 1, y) : GROUND;
  const northPipe = y > 0 ? accessGrid(grid, x, y - 1) : GROUND;
  const eastPipe = x < gridWidth ? accessGrid(grid, x + 1, y) : GROUND;
  const southPipe = y < gridHeight ? accessGrid(grid, x, y + 1) : GROUND;

  if ([EW, NE, SE].includes(westPipe)) {
    connections.push(WEST);
  }
  if ([NS, SW, SE].includes(northPipe)) {
    connections.push(NORTH);
  }
  if ([EW, NW, SW].includes(eastPipe)) {
    connections.push(EAST);
  }
  if ([NS, NW, NE].includes(southPipe)) {
    connections.push(SOUTH);
  }

  if (arrayContains(connections, [NORTH, SOUTH])) {
    return NS;
  } else if (arrayContains(connections, [EAST, WEST])) {
    return EW;
  } else if (arrayContains(connections, [NORTH, EAST])) {
    return NE;
  } else if (arrayContains(connections, [NORTH, WEST])) {
    return NW;
  } else if (arrayContains(connections, [SOUTH, WEST])) {
    return SW;
  } else if (arrayContains(connections, [SOUTH, EAST])) {
    return SE;
  } else {
    console.error("This shouldn't be hit.");
  }
}

function findStartingPoint(grid, rawData) {
  const mazeString = rawData.replace(/\s/g, "");
  const startingPoint = mazeString.indexOf(START);
  const y = Math.floor(startingPoint / grid.length);

  const x = startingPoint % grid[0].length;

  return [x, y];
}

function containsPoint(seenPoints, point) {
  const [x, y] = point ?? [];
  if (!seenPoints || !seenPoints.length || x === undefined || y === undefined) {
    return false;
  }
  return !!seenPoints.find(([seenX, seenY]) => seenX === x && seenY === y);
}

function arrayContains(array, items) {
  return array.filter((entry) => items.includes(entry)).length === items.length;
}

function accessGrid(grid, x, y) {
  return grid[y][x];
}
