const readFile = require("../utils/read-file");
const { parseInt10 } = require("../utils/numbers")

readFile().then(scratchCards);

function scratchCards(data) {
  const lines = data.split(/[\r\n]+/).filter((x) => x);

  const cardStats = lines.reduce((stats, line) => {
    const [, numbers] = line.split(":");
    const [winning, card] = numbers.split("|");
    const winningNumbers = winning
      .split(/[\s]+/)
      .filter((x) => x)
      .map(parseInt10);
    const cardNumbers = card
      .split(/[\s]+/)
      .filter((x) => x)
      .map(parseInt10);

    const yourWinningNumbers = cardNumbers.filter((num) =>
      winningNumbers.includes(num)
    );

    const points = yourWinningNumbers.reduce(
      (total) => (total === 0 ? 1 : total * 2),
      0
    );

    return stats.concat({
      points: points,
      matchingNumbers: yourWinningNumbers.length,
    });
  }, []);

  const initialCardCounts = cardStats.map(() => 0);

  const cardCounts = cardStats.reduce((copies, { matchingNumbers }, index) => {
    copies[index] += 1;

    for (let i = index + 1; i < index + matchingNumbers + 1; i++) {
      copies[i] += copies[index];
    }

    return copies;
  }, initialCardCounts);

  const pointTotal = cardStats.reduce(
    (sum, { points: addend }) => sum + addend,
    0
  );
  const cardsTotal = cardCounts.reduce((sum, addend) => sum + addend);

  console.log(`The pile of cards has a total point value of ${pointTotal}`);
  console.log(`The number of scratch cards you end up with is ${cardsTotal}`);
}
