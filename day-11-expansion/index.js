const readFile = require("../utils/read-file");
const { parseGrid } = require("../utils/input-data");
const { arraySum, parseInt10 } = require("../utils/numbers");

const SPACE = ".";
const GALAXY = "#";

const EXPANSION_SCALE = 1000000 - 1;

readFile().then(expansion);

function expansion(data) {
  const universe = parseGrid(data);

  const expandedUniverse = expandUniverse(universe, 1);
  const oldExpandedUniverse = expandUniverse(universe, EXPANSION_SCALE);
  const galaxies = findGalaxies(expandedUniverse);
  const oldGalaxies = findGalaxies(oldExpandedUniverse);

  const distances = findDistances(galaxies, expandedUniverse);
  const oldDistances = findDistances(oldGalaxies, oldExpandedUniverse);

  const distanceSum = arraySum(distances);
  const oldDistanceSum = arraySum(oldDistances);

  console.log(`The sum of the galaxy lengths is ${distanceSum}`);
  console.log(`The sum of the older galaxy lengths is ${oldDistanceSum}`);
}

function findDistances(galaxies, universe) {
  const distances = [];
  for (let i = 0; i < galaxies.length; i++) {
    const firstGalaxy = galaxies[i];
    for (let j = i + 1; j < galaxies.length; j++) {
      const secondGalaxy = galaxies[j];

      const xStart =
        firstGalaxy[0] < secondGalaxy[0] ? firstGalaxy[0] : secondGalaxy[0];
      const xEnd =
        firstGalaxy[0] < secondGalaxy[0] ? secondGalaxy[0] : firstGalaxy[0];
      const yStart =
        firstGalaxy[1] < secondGalaxy[1] ? firstGalaxy[1] : secondGalaxy[1];
      const yEnd =
        firstGalaxy[1] < secondGalaxy[1] ? secondGalaxy[1] : firstGalaxy[1];

      let distance = 0;
      for (let x = xStart; x < xEnd; x++) {
        const cell = universe[yStart][x];

        const [xDist] = findCellDistance(cell);
        distance += xDist;
      }
      for (let y = yStart; y < yEnd; y++) {
        const cell = universe[y][xEnd];
        const [, yDist] = findCellDistance(cell);
        distance += yDist;
      }

      distances.push(distance);
    }
  }

  return distances;
}

function findCellDistance(cell) {
  return cell === GALAXY ? [1, 1] : cell.split(",").map(parseInt10);
}

function findGalaxies(universe) {
  const galaxies = [];
  universe.forEach((_row, y) => {
    universe[y].forEach((cell, x) => {
      if (cell === GALAXY) {
        galaxies.push([x, y]);
      }
    });
  });
  return galaxies;
}

function expandUniverse(universe, scale = 0) {
  const horizontallyExpanded = horizontallyExpand(universe, scale);
  const verticallyExpanded = verticallyExpand(horizontallyExpanded, scale);

  return verticallyExpanded;
}

function horizontallyExpand(universe, scale) {
  const transposed = transposeUniverse(universe);
  const expanded = transposeUniverse(verticallyExpand(transposed, scale, true));

  return expanded;
}

function verticallyExpand(universe, scale, isHorizontal = false) {
  return universe.reduce((expanded, row) => {
    const hasGalaxy = !!row.find((cell) => cell === GALAXY);
    const encodedRow = row.map((cell) => {
      const currentScale = hasGalaxy ? 1 : scale + 1;
      const [scaleX, scaleY] = cell === SPACE ? "1,1" : cell.split(",");
      if (cell === GALAXY) {
        return cell;
      } else if (isHorizontal) {
        return `${currentScale},${scaleY}`;
      } else {
        return `${scaleX},${currentScale}`;
      }
    });

    expanded.push(encodedRow);
    return expanded;
  }, []);
}

function transposeUniverse(universe) {
  const transposedUniverse = [];

  universe.forEach((_row, y) => {
    universe[y].forEach((cell, x) => {
      if (!transposedUniverse[x]) {
        transposedUniverse[x] = [];
      }
      transposedUniverse[x][y] = cell;
    });
  });

  return transposedUniverse;
}
