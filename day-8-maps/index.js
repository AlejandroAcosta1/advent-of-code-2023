const readFile = require("../utils/read-file");

readFile().then(mapNavigation);

const START_POINT = "AAA";

function mapNavigation(data) {
  const [rawDirections, ...rawMap] = data.split(/\r?\n/).filter((x) => x);

  const directions = Array.from(rawDirections);
  const map = parseMap(rawMap);

  const stepCount = traverse(directions, map);
  const ghostStepCount = ghostTraverse(directions, map);

  console.log(`The number of steps required to reach ZZZ is ${stepCount}`);
  console.log(
    `The number of steps required to reach only nodes that end with Z is ${ghostStepCount}`
  );
}

function traverse(directions, map, startPoint = START_POINT) {
  let location = startPoint;
  let stepCount = 0;
  while (findPointsOfInterest([location], "Z").length < 1) {
    const move = directions[stepCount % directions.length];
    const locationMap = map[location];

    location = locationMap[move];

    stepCount += 1;
  }

  return stepCount;
}

function ghostTraverse(directions, map) {
  const locationList = Object.keys(map);
  const startPoints = findPointsOfInterest(locationList, "A");

  let locations = [...startPoints];

  const stepCounts = locations.map((location) =>
    traverse(directions, map, location)
  );

  const commonStepCounts = findLeastCommonMultiple(stepCounts);

  return commonStepCounts;
}

function isAtTargetLocation(location, targetLetter) {
  return location[2] === targetLetter;
}

function findPointsOfInterest(locations, targetLetter) {
  return locations.filter((location) =>
    isAtTargetLocation(location, targetLetter)
  );
}

function parseMap(mapLines) {
  return mapLines.reduce((lines, currentLine) => {
    const [source, rawDestinations] = currentLine.split(" = ");
    const [left, right] = rawDestinations.split(/[(,) ]/).filter((x) => x);
    return Object.assign(lines, { [source]: { L: left, R: right } });
  }, {});
}

function findLeastCommonMultiple(numbers) {
  return numbers.reduce((previousLcm, number) => lcm(previousLcm, number));
}

function lcm(a, b) {
  return a * (b / gcd(a, b));
}

function gcd(a, b) {
  return !b ? a : gcd(b, a % b);
}
