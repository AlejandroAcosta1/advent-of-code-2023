const { parseInt10 } = require("../utils/numbers");
const readFile = require("../utils/read-file");

readFile().then(boatRace);

function boatRace(data) {
  const [rawTimes, rawDistances] = data.split(/\r?\n/);
  const times = parseValues(rawTimes);
  const records = parseValues(rawDistances);

  const correctedRaceTime = parseValues(rawTimes.replace(/\s+/g, ""));
  const correctedRecordTime = parseValues(rawDistances.replace(/\s+/g, ""));

  const raceVictories = times.map((time, raceIndex) => {
    const recordDistance = records[raceIndex];
    return findVictory(time, recordDistance);
  });

  const correctedRaceVictories = correctedRaceTime.map((time, raceIndex) => {
    const recordDistance = correctedRecordTime[raceIndex];
    return findVictory(time, recordDistance);
  });

  const raceProduct = raceVictories.reduce(
    (product, multiplicand) => product * multiplicand
  );

  console.log(`The record beating race times product is ${raceProduct}`);
  console.log(
    `You can beat the much longer race this many times: ${correctedRaceVictories[0]}`
  );
}

function findVictory(raceTime, recordDistance) {
  let victories = 0;
  for (let boatSpeed = 1; boatSpeed < raceTime; boatSpeed++) {
    const raceDistance = boatSpeed * (raceTime - boatSpeed);
    if (raceDistance > recordDistance) {
      victories += 1;
    }
  }
  return victories;
}

function parseValues(valueString) {
  return valueString
    .split(":")[1]
    .split(/\s+/)
    .filter((x) => x)
    .map(parseInt10);
}
