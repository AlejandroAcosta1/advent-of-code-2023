const readFile = require("../utils/read-file");

readFile().then(camelPoker);

const FIVE_OF_A_KIND = "FIVE_OF_A_KIND";
const FOUR_OF_A_KIND = "FOUR_OF_A_KIND";
const THREE_OF_A_KIND = "THREE_OF_A_KIND";
const FULL_HOUSE = "FULL_HOUSE";
const TWO_PAIR = "TWO_PAIR";
const ONE_PAIR = "ONE_PAIR";
const HIGH_CARD = "HIGH_CARD";

const cardPrecedence = ["A", "K", "Q", "J", "T", "9", "8", "7", "6", "5", "4", "3", "2"];
const jokerCardPrecedence = ["A", "K", "Q", "T", "9", "8", "7", "6", "5", "4", "3", "2", "J"];
const handPrecedence = [
  FIVE_OF_A_KIND,
  FOUR_OF_A_KIND,
  FULL_HOUSE,
  THREE_OF_A_KIND,
  TWO_PAIR,
  ONE_PAIR,
  HIGH_CARD,
];

function camelPoker(data) {
  const hands = data
    .split(/\r?\n/)
    .filter((x) => x)
    .map((line) => line.split(" "));

  const sortedHands = [...hands].sort(sortCamelPoker).reverse();
  const sortedJokerHands = [...hands].sort(sortJokerPoker).reverse();

  const handRankings = rankHands(sortedHands);
  const jokerHandRankings = rankHands(sortedJokerHands)

  const totalWinnings = handRankings.reduce((sum, addend) => sum + addend);
  const totalJokerWinnings = jokerHandRankings.reduce((sum, addend) => sum + addend);

  console.log(`The total winnings are ${totalWinnings}`);
  console.log(`The total winnings with the joker rule are ${totalJokerWinnings}`);
}

function rankHands(hands) {
  return hands.map((hand, index) => {
    const rank = index + 1;
    const [, bid] = hand;
    return parseInt(bid) * rank;
  })
}

function sortCamelPoker(aHand, bHand) {
  const [firstHand] = aHand;
  const [secondHand] = bHand;

  const firstHandType = handType(firstHand);
  const secondHandType = handType(secondHand);

  const firstHandRanking = handPrecedence.indexOf(firstHandType);
  const secondHandRanking = handPrecedence.indexOf(secondHandType);

  if (firstHandRanking !== secondHandRanking) {
    return firstHandRanking - secondHandRanking;
  }

  for (let i = 0; i < firstHand.length; i++) {
    const firstOrdering = cardPrecedence.indexOf(firstHand[i]);
    const secondOrdering = cardPrecedence.indexOf(secondHand[i]);

    if (firstOrdering !== secondOrdering) {
      return firstOrdering - secondOrdering;
    }
  }

  return 0;
}

function sortJokerPoker(aHand, bHand) {
  const [firstHand] = aHand;
  const [secondHand] = bHand;

  const firstHandType = jokerHandType(firstHand);
  const secondHandType = jokerHandType(secondHand);

  const firstHandRanking = handPrecedence.indexOf(firstHandType);
  const secondHandRanking = handPrecedence.indexOf(secondHandType);

  if (firstHandRanking !== secondHandRanking) {
    return firstHandRanking - secondHandRanking;
  }

  for (let i = 0; i < firstHand.length; i++) {
    const firstOrdering = jokerCardPrecedence.indexOf(firstHand[i]);
    const secondOrdering = jokerCardPrecedence.indexOf(secondHand[i]);

    if (firstOrdering !== secondOrdering) {
      return firstOrdering - secondOrdering;
    }
  }

  return 0;
}

function handType(hand) {
  const matchingCardCounts = Array.from(hand)
    .map((card) => hand.split(card).length - 1)
    .filter((x) => x > 1);

  if (matchingCardCounts[0] === 5) {
    return FIVE_OF_A_KIND;
  } else if (matchingCardCounts[0] === 4) {
    return FOUR_OF_A_KIND;
  } else if (matchingCardCounts.includes(3) && matchingCardCounts.includes(2)) {
    return FULL_HOUSE;
  } else if (matchingCardCounts[0] === 3) {
    return THREE_OF_A_KIND;
  } else if (matchingCardCounts[0] === 2 && matchingCardCounts.length === 4) {
    return TWO_PAIR;
  } else if (matchingCardCounts[0] === 2 && matchingCardCounts.length === 2) {
    return ONE_PAIR;
  } else {
    return HIGH_CARD;
  }
}

function jokerHandType(hand) {
  const jokerCount = (hand.match(/J/g) || []).length;

  const baseHandType = handType(hand)

  if (jokerCount === 0) {
    return baseHandType;
  }


  if (
    baseHandType === FOUR_OF_A_KIND && (jokerCount === 4 || jokerCount === 1) ||
    baseHandType === FULL_HOUSE && (jokerCount === 2 || jokerCount === 3)
  ) {
    return FIVE_OF_A_KIND;
  }
  else if (
    baseHandType === FOUR_OF_A_KIND && jokerCount === 4 ||
    baseHandType === THREE_OF_A_KIND && (jokerCount === 1 || jokerCount === 3) ||
    baseHandType === TWO_PAIR && jokerCount === 2
  ) {
    return FOUR_OF_A_KIND;
  }
  else if (baseHandType === TWO_PAIR && jokerCount === 1) {
    return FULL_HOUSE;
  }
  else if (baseHandType === ONE_PAIR && (jokerCount === 2 || jokerCount === 1)) {
    return THREE_OF_A_KIND;
  }
  else if (baseHandType === HIGH_CARD && jokerCount === 1) {
    return ONE_PAIR
  }

  return baseHandType;
}