const readFile = require("../utils/read-file");

readFile().then(trebuchet);

function trebuchet(data) {
  const lines = data.split("\n");

  const calibrationSum = lines.reduce((sum, line) => {
    if (!line) {
      return sum;
    }
    const calibrationNumbers = line.replace(/[a-z]/g, "");
    const firstDigit = calibrationNumbers[0];
    const lastDigit = calibrationNumbers.slice(-1);
    const calibrationValue = parseInt(firstDigit) * 10 + parseInt(lastDigit);
    return sum + calibrationValue;
  }, 0);

  const realCalibrationSum = lines.reduce((sum, line) => {
    if (!line) {
      return sum;
    }

    const matcher = /(?=(\d|one|two|three|four|five|six|seven|eight|nine))/g;
    const digits = Array.from(line.matchAll(matcher), (x) => x[1]).map(
      (digit) => {
        switch (digit) {
          case "one":
            return 1;
          case "two":
            return 2;
          case "three":
            return 3;
          case "four":
            return 4;
          case "five":
            return 5;
          case "six":
            return 6;
          case "seven":
            return 7;
          case "eight":
            return 8;
          case "nine":
            return 9;
          default:
            return parseInt(digit);
        }
      }
    );

    const firstDigit = digits[0];
    const lastDigit = digits.slice(-1)[0];
    const calibrationValue = firstDigit * 10 + lastDigit;
    console.log({
      digits,
      firstDigit,
      lastDigit,
      sum,
      line,
      calibrationValue,
      newSum: sum + calibrationValue,
    });
    return sum + calibrationValue;
  }, 0);

  console.log(`The sum of all calibration values is ${calibrationSum}`);
  console.log(
    `The real sum of all calibration values is ${realCalibrationSum}`
  );
}
